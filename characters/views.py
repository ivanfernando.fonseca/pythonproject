from django.contrib.auth.decorators import login_required
from characters.models import Character
from django.shortcuts import render

# Create your views here.

@login_required
def list(request):
    list=Character.objects.all()
    print(list)
    return render(request,'marvel/index.html', {'characters':list})
