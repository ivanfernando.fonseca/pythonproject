from django.db import models


class Character(models.Model):
    name = models.CharField(max_length=50)
    description=models.CharField(max_length=500)
    image = models.CharField(max_length=200)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'characters'




