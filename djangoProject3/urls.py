from django.contrib import admin
from django.http import HttpResponse
from django.urls import path
from characters import views as views_characters
from users import views as users_views

def principal(request):
    return HttpResponse("desplegando aplicaciones en docker")


urlpatterns = [
    path('',principal),
    path('admin/', admin.site.urls),
    path('list/', views_characters.list,name='marvel_characters'),
    path('users/login',users_views.login_v,name='login'),

]
