from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect

def login_v(request):
    if request.method=='POST':
        username=request.POST['username']
        password=request.POST['password']
        user=authenticate(request, username=username,password=password)
        print(user)

        if user:
            login(request,user)
            return redirect('marvel_characters')
        else:
             return render(request,'users/login.html',{'error':'invalid username or password'})
    return render(request,'users/login.html')

